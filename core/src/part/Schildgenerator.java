package part;

import Scenes.PartMenu;
import model.Ship;

/**
 * @author s.fontana
 *
 */
public class Schildgenerator extends Part{
	int shield;
	int econ;
	public Schildgenerator(PartMenu pm, Ship ship, String part, int material,int gridx,int gridy, float rot) {
		super(pm, ship, part, material, gridx, gridy, rot, null);
		switch (material) {
		case 1:
			econ=20;
			shield=45;
			mass=30;
			hpmax=5;
			break;
		case 2:
			econ=40;
			shield=90;
			mass=60;
			hpmax=10;				
			break;

		case 3:
			econ=80;
			shield=150;
			mass=90;
			hpmax=15;
			break;

		case 4:
			econ=160;
			shield=300;
			mass=180;
			hpmax=30;
			break;
		}
		hp = hpmax;
	}
	@Override
	public void add(Ship ship, int material) {
		ship.setEnergyconsumption(ship.getEnergyconsumption()+econ);
		ship.setShield(ship.getShield()+shield);
		ship.setMass(ship.getMass()+mass);
		ship.setHp(ship.getHp()+hpmax);
		
	}
	

}
