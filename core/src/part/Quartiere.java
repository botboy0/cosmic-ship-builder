package part;

import Scenes.PartMenu;
import model.Ship;

public class Quartiere extends Part{
	int crew;
	int econ;
	public Quartiere(PartMenu pm, Ship ship, String part, int material,int gridx,int gridy, float rot) {
		super(pm, ship, part, material, gridx, gridy, rot, null);
		switch (material) {
		case 1:
			econ=10;
			crew=4;
			mass=10;
			hpmax=20;
			break;
		case 2:
			econ=20;
			crew=6;
			mass=20;
			hpmax=40;				
			break;

		case 3:
			econ=40;
			crew=8;
			mass=30;
			hpmax=60;
			break;

		case 4:
			econ=10;
			crew=12;
			mass=60;
			hpmax=120;
			break;
		}
		hp = hpmax;
	}

	@Override
	public void add(Ship ship, int material) {
		ship.setMass(ship.getMass()+mass);
		ship.setHp(ship.getHp()+hpmax);
		ship.setCrew(ship.getCrew()+crew);
		ship.setEnergyconsumption(ship.getEnergyconsumption()+econ);
		
	}

	
}
