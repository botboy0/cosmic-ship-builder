package part;

import Scenes.PartMenu;
import model.Ship;

public class Batterie extends Part {
	int bat;
	public Batterie(PartMenu pm, Ship ship, String part, int material,int gridx,int gridy, float rot) {
		super(pm, ship, part, material, gridx, gridy, rot, null);
		switch (material) {
		case 1:
			bat=500;
			mass=5;
			hpmax=5;
			break;
		case 2:
			bat=1000;
			mass=10;
			hpmax=10;				
			break;

		case 3:
			bat=2000;
			mass=15;
			hpmax=15;
			break;

		case 4:
			bat=2000;
			mass=30;
			hpmax=30;
			break;
		}
		hp = hpmax;
	}
	@Override
	public void add(Ship ship, int material) {
		ship.setBattery(ship.getBattery()+bat);
		ship.setMass(ship.getMass()+mass);
		ship.setHp(ship.getHp()+hpmax);
		
		
	}
	
}
