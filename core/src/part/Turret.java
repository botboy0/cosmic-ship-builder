package part;

import com.hgs.ShipBuilder;

import Scenes.PartMenu;
import model.Ship;

public class Turret extends Part{
	int damage;
	int econ;
	public static float hpmax;
	public Turret(PartMenu pm, Ship ship, String part, int material, int gridx, int gridy, float rot) {
		super(pm, ship, part, material, gridx, gridy, rot, null);
		switch (material) {
		case 1:
			hpmax = 10;
			econ=10;
			mass=5;
			damage=10;
			break;
		case 2:
			hpmax = 20;
			econ=20;
			mass=10;
			damage=20;
			break;

		case 3:
			hpmax = 30;
			econ=40;
			mass=15;
			damage=40;
			break;

		case 4:
			hpmax = 60;
			econ=40;
			mass=30;
			damage=10;
			break;
		}
		hp = hpmax;
	}
	@Override
	public void add(Ship ship, int material) {
		ship.setEnergyconsumption(ship.getEnergyconsumption()+econ);
		ship.setMass(ship.getMass()+mass);
		ship.setHp(ship.getHp()+hpmax);	
	}
	
}
