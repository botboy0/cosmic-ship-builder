package part;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.hgs.ShipBuilder;

import Scenes.PartMenu;
import model.Ship;

public abstract class Part {
public ShipBuilder sb;
public PartMenu pm;
Ship ship;
int gridx;
int gridy;
Sprite sprite;
float rotation;
int mass;
float hpmax;
float hp;
String part;
int material;
float posx;
float posy;
public Part(PartMenu pm, Ship ship, String part, int material, int gridx, int gridy, float rot, ShipBuilder sb) {
	this.sb = sb;
	this.ship = ship;
	this.pm = pm;
	this.part = part;
	this.material = material;
	this.gridx = gridx;
	this.gridy = gridy;
	this.rotation = rot;
	this.sprite = new Sprite(ShipBuilder.gameskin.getRegion(part+"_"+String.valueOf(material)));
	this.sprite.setRotation(rotation);
	posx= ((gridx+11)* ShipBuilder.HEIGHT / 47.25f + ShipBuilder.HEIGHT / 283.5f);
	posy= -((gridy-35)* ShipBuilder.HEIGHT / 47.25f - ShipBuilder.HEIGHT / 113.4f);
	this.sprite.setPosition(posx,posy);
	
	
}


public Sprite getSprite() {
	return sprite;
}

public void setSprite(Sprite sprite) {
	this.sprite = sprite;
}

public float getRotation() {
	return rotation;
}

public void setRotation(float rotation) {
	this.rotation = rotation;
}

public String getPart() {
	return part;
}

public int getMaterial() {
	return material;
}
public abstract void add(Ship ship,int material);

public int getGridx() {
	return gridx;
}

public void setGridx(int gridx) {
	this.gridx = gridx;
}

public int getGridy() {
	return gridy;
}

public void setGridy(int gridy) {
	this.gridy = gridy;
}
@Override
	public String toString() {
		String info="Part: "  +part  +"\n"
				   +"material: " +material +"\n"
				   +"hpmax: " +hpmax +"\n"
				   +"hp: "     + hp  +"\n";
		return info;
	}


public float getHp() {
	return hp;
}


public float getHpmax() {
	return hpmax;
}


public boolean modifyHp(float hp) {
	this.hp += hp;
	if (this.hp>0) {
		return false;
	}else {
		return true;
	}
}

}
