package part;

import Scenes.PartMenu;
import model.Ship;

public class Antrieb extends Part{
	int schub;
	int econ;
	public Antrieb(PartMenu pm, Ship ship, String part, int material,int gridx,int gridy, float rot) {
		super(pm, ship, part, material, gridx, gridy, rot, null);
		switch (material) {
		case 1:
			econ=10;
			mass=5;
			hpmax=10;
			schub=200;
			break;
		case 2:
			econ=20;
			mass=10;
			hpmax=20;
			schub=400					;
			break;

		case 3:
			econ=40;
			mass=15;
			hpmax=30;
			schub=600;
			break;

		case 4:
			econ=40;
			mass=30;
			hpmax=60;
			schub=700;
			break;
		}
		hp = hpmax;
	}

	@Override
	public void add(Ship ship, int material) {
		ship.setEnergyconsumption(ship.getEnergyconsumption()+econ);
		ship.setMass(ship.getMass()+mass);
		ship.setHp(ship.getHp()+hpmax);
		ship.setThrust(ship.getThrust()+schub);
		
		
	}

}
