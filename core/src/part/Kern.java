package part;

import Scenes.PartMenu;
import model.Ship;

public class Kern extends Part{
int partsmax;
int econ;
	public Kern(PartMenu pm, Ship ship, String part, int material,int gridx,int gridy, float rot) {
		super(pm, ship, part, material, gridx, gridy, rot, null);
	
		
		switch (material) {
		case 1:
			partsmax=50;
			break;
		case 2:
			partsmax=100;
			break;

		case 3:
			partsmax=200;
			break;

		case 4:
			partsmax=500;
			break;
		}
		switch (material) {
		case 1:
			hpmax= 100;
			mass=20;
			partsmax=50;
			econ=50;
			break;
		case 2:
			hpmax= 200;
			mass=40;
			partsmax=100;
			econ=100;
			break;

		case 3:
			hpmax =300;
			mass=100;
			partsmax=200;
			econ=200;
			break;

		case 4:
			hpmax= 600;
			mass=200;
			partsmax=500;
			econ=200;
			break;
		}
		hp = hpmax;
	}

	@Override
	public void add(Ship ship, int material) {
		ship.setHp(ship.getHp()+hpmax);																				
		ship.setMass(ship.getMass()+mass);
		ship.setPartsmax(partsmax);
		ship.setEnergyconsumption(ship.getEnergyconsumption()+econ);
	}

	public int getPartsmax() {
		return partsmax;
	}


}
