package model;

/**
 * Filterung der Kollisionen andhand von Gruppen die durch Bitmaskierungen gekennzeichnet werden
 */
public class Box2dFILTER {

	/** Die Konstante zur Bestimmung eines kollidierenden Teiles */
	public static final short BIT_PART =1;
	
	/** Die Konstante zur Bestimmung eines kollidierenden Asteroidenteiles */
	public static final short BIT_ASTERODIPART =2;
	
	public static final short BIT_LASER =4;
	
}
