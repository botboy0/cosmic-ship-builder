package model;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.hgs.ShipBuilder;

/**
 * Simple Klasse die ein Event ausl�st wenn der Spieler einen g�ltigen Schiffnamen ausgew�hlt hat mit dem das Schiff erzeugt werden kann
 */
public class Confirmlistener extends ChangeListener{
	 
 	/** Das Fesnter der Namenswahl */
 	Window w;
	 
 	/** Das Textfeld der Namenswahl */
 	TextField tf;
	 
 	/** Zugriff auf die Hauptinstanz des spiels*/
 	ShipBuilder sb;
	
	/**
	 * Initialisiert die ben�tigten Daten wie: 
	 *
	 * @param nameinput der eingegebene Name
	 * @param textfield das Textfeld
	 * @param sb die Hauptinstanz des Spiels
	 */
	public Confirmlistener(Window nameinput, TextField textfield,ShipBuilder sb) {
		this.w = nameinput;
		this.tf = textfield;
		this.sb = sb;
		
	}

	/** 
	 * Event das bei korrekter Namenseingabe und dr�cken des Eingabe Knopfes ausgel�st wird und daraufhin die erstellung des Schiffs bewirkt, 
	 * das Schiff automatisch speichert und das Fenster schlie�t
	 */
	@Override
	public void changed(ChangeEvent event, Actor actor) {
		if (tf.getText().length()>0) {
			sb.es.createship(tf.getText());
			sb.es.storage.saveship();
			w.remove();
		}
		
		
	}
	
}
