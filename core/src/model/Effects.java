package model;

import java.util.ArrayList;

import org.omg.CORBA.OMGVMCID;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;

import net.dermetfan.gdx.graphics.g2d.Box2DSprite;

 /** Die Klasse Effects ist eine selbsterstelle Klasse in der Sound sowie  Partikel effekte definiert sind  */
public class Effects {

/** Die Hintergrundmusik des Hauptmen�s*/
public Music bgmusic;

/** Der  Ton der bei einer Kollision mit einem Asteroiden abgespielt wird*/
public Sound collisionsound;

/** Der Partikeleffekt der Kollision , welcher eine kleine Explosion aus quadraten darstellt */
public ParticleEffect collisionparticle;

/** Die Liste aller Partikeleffekte die zum gemeinsamen Zeichnen und aktualisieren aller Partikel ben�tigt wird */
public ArrayList<ParticleEffect> particlelist;

public static ArrayList<Turretdata> beamdata= new ArrayList<Turretdata>();
/**
 * Initialisiert die Effekte mit den Dateipfaden des Asset Ordners
 */
public Effects() {
	for (int i = 1; i <= 4; i++) {
		Turretdata data= new Turretdata();
		data.sprite = new Box2DSprite(new Texture(Gdx.files.internal("laserbeam_"+i+".png")));
		data.sprite.setZIndex(10);
		beamdata.add(data);
	}
	bgmusic = Gdx.audio.newMusic(Gdx.files.internal("bgmusic.ogg"));
	collisionsound = Gdx.audio.newSound(Gdx.files.internal("crash.ogg"));
	particlelist = new ArrayList<ParticleEffect>(10);
	 collisionparticle = new ParticleEffect();
	 collisionparticle.load(Gdx.files.internal("collision.p"),Gdx.files.internal(""));
	 particlelist.add(collisionparticle);
}

/**
 * Erzeugt einen Partikeleffekt an einer bestimmten Stelle
 *
 * @param name Der Name des Effekts
 * @param x the Die X Position des Effekts
 * @param y the Die Y Position des Effekts
 * @param intensity Die Itensit�t des Effekts
 */
public void spawnparticle(String name , float x ,float y,float intensity){
	    collisionparticle = new ParticleEffect();
	    collisionparticle.load(Gdx.files.internal("collision.p"),Gdx.files.internal(""));
		collisionparticle.setPosition(x, y);
		collisionparticle.scaleEffect(intensity);
		collisionparticle.start();
		particlelist.add(collisionparticle);
		if (particlelist.size()>50) {
			particlelist.remove(0);
		}
		
	
	}
}
