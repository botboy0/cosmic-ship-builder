package model;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import part.Part;

/**
 * Auf tiefensuche basierender Algorithmus zur festellung zusammenh�nnger Daten in einem 2 Dimensionalem Array, einmal f�r die Nutzung im Editor zur Kontrolle 
 * der Integrit�t und des Zusammenhangs. 
 * 
 * Und im Spiel um das vollst�ndige Zerst�ren nichtmehr zusammenh�ngender Schiffsteile zu sichern.
 *
 */
public class ConnectedAlg {
	
	/** Die kaputten Teile des Schiff im Spiel */
	ArrayList<Array<Vector2>> brokenparts;
	
	/** Anzahl der "Inseln"-zusammenh�ngende integer Daten die in der Matrix durch einseitige Bewung nach oben, unten , rechts oder links erreicht werden kann */
	int numIslands = 0;
	
	/** Leere Zellen werden mit 1 Markiert , noch nicht besuchte , verf�gbare Zellen mit 1 und schon besuchte mit 2 */
	private final int VISITED = 2;

  
    /** Die Spalten */
    private final int[] columns = { 0,-1, 1, 0};
    
    /** Die Reihen */
    private final int[] rows =    {-1, 0, 0, 1};
   
    /**
     * Finde die Anzahl der Inseln
     *
     * @param matrix Die �bergebene Matrix die eine Integerdarstellung des Schiffes ist
     * @return Die Anzahl der Inseln zur bestimmung ob mehr als ein Zusammenh�nger Block and Teilen besteht
     */
    public int findIslandsCount(int[][] matrix) {

        int numIslands = 0;

        for(int i=0; i<matrix.length; i++) {
            for(int j=0; j<matrix[i].length; j++) {
                if(matrix[i][j]!=VISITED && matrix[i][j] == 1) {
                    dfs(i,j, matrix);
                    numIslands++;
                }
                else {
                    matrix[i][j]=VISITED;
                }
            }
        }

        return numIslands;
    }
    
    /**
     * Tiefensuche �berpr�ft f�r jede einzelne Stelle ob jede andere Stelle erreicht werden kann und ruft sich selbst rekursiv auf
     *
     * @param i Die position i
     * @param j Die position j
     * @param matrix Die �bergebene repr�sentaiton des Schiffes als Integer Array
     */
    private void dfs(int i, int j, int[][] matrix) {
        matrix[i][j] = VISITED;

      
        for(int n=0; n<columns.length; n++) {

            int c = j+columns[n];
            int r = i+rows[n];

            if(isLegal(c, r, matrix)) {
                dfs(r, c, matrix);
            }
        }
    }
    
    /**
     * Nach dem selbem Muster arbeitend sucht findbrokenparts nicht die Anzahl der Inseln sonder erstellt eine Liste 
     * aller zu zerst�renden Teile die nichtmehr mit dem Kern des Schiffes verbunden sind
     *
     * @param matrix the matrix
     * @param model Die logische Darstellung des Schiffs
     * @return Die Liste aller zu zerst�renden Teile
     */
    public ArrayList<Array<Vector2>> findbrokenparts(int[][] matrix,Part[][]model) {
    	brokenparts=new ArrayList<Array<Vector2>>();
    	numIslands=0;
        for(int i=0; i<matrix.length; i++) {
            for(int j=0; j<matrix[i].length; j++) {
                if(matrix[i][j]!=VISITED && matrix[i][j] == 1) {
                	
                	brokenparts.add(new Array<Vector2>());
                    brokensearch(i,j, matrix);
                    numIslands++;
                    
                }
                else {
                    matrix[i][j]=VISITED;
                }
            }
        }
        for (int i = 0; i < brokenparts.size(); i++) {
			for (int j = 0; j < brokenparts.get(i).size; j++) {
				if (model[(int) brokenparts.get(i).get(j).x][(int) brokenparts.get(i).get(j).y].getPart().equals("kern")) {
					brokenparts.remove(i);
					return brokenparts;
				}
			}
		}
        return brokenparts;
    }
    
    /**
     * Eine abgewandelte Version der tiefensuche die nicht nach der gesamtzahl aller Inseln sucht sondern der Gesamtzahl aller zu zerst�renden Teile
     *
     * @param i Die position i
     * @param j Die position j
     * @param matrix Die �bergebene repr�sentaiton des Schiffes als Integer Array
     */
    private void brokensearch(int i, int j, int[][] matrix) {
        matrix[i][j] = VISITED;
        brokenparts.get(numIslands).add(new Vector2(i,j));
       
        
      
        for(int n=0; n<columns.length; n++) {

            int c = j+columns[n];
            int r = i+rows[n];

            if(isLegal(c, r, matrix)) {
                brokensearch(r, c, matrix);
            }
        }
    }
   
    /**
     * �berpr�fen ob das zu begehende Feld zul�ssig ist
     *
     * @param c Die position c
     * @param r DIe position r
     * @param matrix Die �bergebene repr�sentaiton des Schiffes als Integer Array
     * @return Ergebnis der G�ltigkeitspr�fung wird zur�ckgegeben
     */
    private boolean isLegal(int c, int r, int[][] matrix) {
        return c>=0 && r>=0 && r<matrix.length && c<matrix[0].length && matrix[r][c]!=VISITED && matrix[r][c]==1;
    }
	
   
    
    
}
