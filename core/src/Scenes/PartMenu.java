package Scenes;

import java.util.ArrayList;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.hgs.ShipBuilder;
import model.Partlistener;

public class PartMenu {
	ShipBuilder sb;
	public Stage stage;
	Table table;
	int page =2;
	public String[]parttypes= new String[] {"antrieb","batterie",
			"dreieck","generator","kern",
			"panzerung","quartiere",
			"schildgenerator","spitze",
			"steuerung","turret"};
	
	ArrayList<Button[]>btnsets=new ArrayList<Button[]>();
	
	
	Button[] prevbtns= new Button[4];
	Button[] nextbtns= new Button[4];
	String Selectedpart;
	boolean partSelected=false;
	
	TextureAtlas partatlas;
	public Image partbox = new Image(new Texture("partbox.png"));
	
	public PartMenu(final ShipBuilder sb) {
		this.sb = sb;
		for (int i = 0; i < 4; i++) {
			btnsets.add(new Button[11]);
		}
		Gdx.input.setInputProcessor(stage);
		texturereg(btnsets);
		
		stage = new Stage();
		partbox.setSize(ShipBuilder.WIDTH*0.9f, ShipBuilder.HEIGHT/10);
		partbox.setPosition(ShipBuilder.WIDTH*0.05f, ShipBuilder.HEIGHT-partbox.getHeight());
	    table = new Table();
		table.setSize(ShipBuilder.WIDTH*0.9f, ShipBuilder.HEIGHT/10);
		table.top();
		table.setPosition(ShipBuilder.WIDTH*0.05f, ShipBuilder.HEIGHT*0.9f);
		
		reload();
		
		for (Button prev : prevbtns) {
			prev.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					if (!sb.es.isIndialog()) {
						if (sb.es.ship!=null) {
							if (page>1) {
								page--;
							}else {
								page=4;
							}
							reload();	
						}
					}
					
					
				}
	        });
		}
		
		for (Button next : nextbtns) {
			next.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					if (!sb.es.isIndialog()) {
						if (sb.es.ship!=null) {
							if (page<4) {
								page++;
							}else {
								page=1;
							}
							reload();
						}
					}
					
					
				}
				
	        });
		}
				
		
	}
	
	private void reload() {
		table.clear();
		stage.clear();
		stage.addActor(partbox);
		stage.addActor(table);
		
		prevbtns[page-1].setBounds(0, ShipBuilder.HEIGHT*0.9f, ShipBuilder.HEIGHT*0.1f, ShipBuilder.HEIGHT*0.1f);
		nextbtns[page-1].setBounds(ShipBuilder.WIDTH*0.94f, ShipBuilder.HEIGHT*0.9f, ShipBuilder.HEIGHT*0.1f, ShipBuilder.HEIGHT*0.1f);
		
		
		stage.addActor(prevbtns[page-1]);
		stage.addActor(nextbtns[page-1]);
		
					for (Button btn : btnsets.get(page-1)) {
						table.add(btn).expandX().padTop(ShipBuilder.HEIGHT/45).minSize(60);
					}
		
	}
	
	private void texturereg(ArrayList<Button[]> btnlist) {
		int btnsetnumber = 1;
		
		for (int i = 0; i < 4; i++) {
			prevbtns[i]= new Button(new TextureRegionDrawable(ShipBuilder.gameskin.getRegion("prev"+"_"+String.valueOf(i+1))));
			nextbtns[i]= new Button(new TextureRegionDrawable(ShipBuilder.gameskin.getRegion("next"+"_"+String.valueOf(i+1))));
			
		}
		
		for (Button[] buttons : btnlist) {
			for (int i = 0; i < buttons.length; i++) {
				buttons[i]= new Button(new TextureRegionDrawable(ShipBuilder.gameskin.getRegion(parttypes[i]+"_"+String.valueOf(btnsetnumber))),
									   new TextureRegionDrawable(ShipBuilder.gameskin.getRegion(parttypes[i]+"_"+String.valueOf(btnsetnumber))));
				
				buttons[i].addListener(new Partlistener(parttypes[i]+"_"+String.valueOf(btnsetnumber),this,sb));
			}
			
			btnsetnumber++;
		}
		
	}
	public String getSelectedpart() {
		return Selectedpart;
	}
	public void setSelectedpart(String selectedpart) {
		Selectedpart = selectedpart;
	}
	
	public boolean isPartSelected() {
		return partSelected;
	}
	public void setPartSelected(boolean partSelected) {
		this.partSelected = partSelected;
	}	
	
	
}
