package Scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.hgs.ShipBuilder;
import model.Player;
import model.ShipStorage;

public class Shipselect extends Window {
	ShipBuilder sb;
	Window w;
	ShipStorage storage;
	FileHandle selected;
	public List<String> selectlist;
	
	public Shipselect(String title, Skin skin,final ShipBuilder sb ) {
		super(title, skin);
		//this.debug();
		this.sb = sb;
		this.setBounds(ShipBuilder.WIDTH/2-ShipBuilder.WIDTH/8, ShipBuilder.HEIGHT/2-ShipBuilder.HEIGHT/4, ShipBuilder.WIDTH/4, ShipBuilder.HEIGHT/2);
		this.setResizable(false);
		this.setMovable(false);
		w=this;
		storage = new ShipStorage(sb);
		Image backgroundimage = new Image(new Texture(Gdx.files.internal("infopanel2.png")));
		Stack browserwindow= new Stack();
	    selectlist = new List<String>(skin);
	    selectlist.addListener(new ChangeListener(){@Override
	    public void changed(ChangeEvent event, Actor actor) {
	    	if (selectlist.getSelected()!=null&&selectlist.getSelected().length()>0) {
				selected = Gdx.files.local("ships/"+selectlist.getSelected()+".ship");
			}
	    }});
	    Table background = new Table(skin);
		final ScrollPane container= new ScrollPane(selectlist);
		container.setWidth(50);
		final Slider scrollbar = new Slider(0, 100, 1, true, skin);
		scrollbar.setValue(100);
		scrollbar.getStyle().knob.setMinWidth(this.getWidth()/15);
		scrollbar.getStyle().knob.setMinHeight(this.getHeight()/15);
		scrollbar.getStyle().background.setMinWidth(this.getWidth()/15);
		scrollbar.addListener(new ChangeListener() {	
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				container.setScrollPercentY(1-scrollbar.getPercent());
				container.updateVisualScroll();
				
				
			}});
		TextButton select= new TextButton("Select", skin);
		select.addListener(new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				w.setVisible(false);
				sb.gs.setPlayer(new Player(sb, sb.gs.getWorld(), storage.loadship(selected)));
				sb.gs.init();
				
			}
		});
		
		    this.top();
		    background.add(backgroundimage).size(Value.percentWidth(1f, this),Value.percentHeight(0.86f,this)).padLeft(this.getWidth()/20);
		    browserwindow.add(container);
		    browserwindow.addActorBefore(container,background);
		    background.scaleBy(4);
		    this.add(browserwindow).size(Value.percentWidth(0.75f, this),Value.percentHeight(0.75f, this)).top().padTop(this.getHeight()/20).padLeft(this.getWidth()/20);
		    this.add(scrollbar).size(Value.percentWidth(.03F, this),Value.percentHeight(0.72f, this)).top().padTop(this.getHeight()/15).padRight(this.getWidth()/100);
		    this.row();
		    this.add(select).top().size(this.getWidth()*0.95f, this.getHeight()*0.1f).colspan(2).padTop(this.getHeight()/17);
		    storage.update(selectlist);
		  
		
	}
	@Override
	protected void drawBackground(Batch batch, float parentAlpha, float x, float y) {
		
		super.drawBackground(batch, parentAlpha, x, y-ShipBuilder.HEIGHT/60);
		
	
	}
	public ShipStorage getStorage() {
		return storage;
	}
	public FileHandle getSelected() {
		return selected;
	}

}
