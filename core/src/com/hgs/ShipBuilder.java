package com.hgs;

import java.awt.Dimension;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.physics.box2d.Box2D;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import Screens.EditorScreen;
import Screens.GameScreen;
import Screens.MenuScreen;
import model.Effects;

/**
 * Hauptklasse des Spiels von der alle Teilbereiche initialisiert werden.
 */
public class ShipBuilder extends Game {
	
	/** Festlegung der Bildschirmgr��e las Bildschrimdimension durch verwendung des Java toolkits. 
	 * Wird von Fenstern ben�tigt um relative Texturgr��en zu erm�glichen */
	public static Dimension screendimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
	
	/** Die erkannte breite des Bildschirms die von allen Fenstern genutzt werden kann */
	public static final int WIDTH =screendimension.width;
	
	/** Die erkannte h�he des Bildschirms die von allen Fenstern genutzt werden kann */
	public static final int HEIGHT = screendimension.height;
	
	/** Der Spritebatch  ist eine Sammlung die von dem renderer auf der GPU abgearbeitet und gezeichnet wird*/
	public SpriteBatch batch;
	
	/** Die Klasse Effects ist eine selbsterstelle Klasse in der Sound sowie  Partikel effekte definiert sind */
	public static Effects effects;
	
	/** Der gameskin ist die zentrale Sammlung  die alle Benutzerinterface texturen h�lt um sie diesen bei initialisierung bereitzustellen.
	 * Dabei bietet der Datentyp Skin einen effizienten Weg gro�e Mengen an assets komprimiert und ohne lange Dateipfade zug�nglich zu machen*/
	public static Skin gameskin;
	
	/** Der Editorscreen ist das Hauptfenster des Editors, in dem  die Raumschiffe durch den Benutzer gebaut werden k�nnen*/
	public EditorScreen es;
	
	/** Der Gamescreen ist das Hauptfenster des Spiels in dem man seine selbstgebauten Schiffskreationen austesten kann */
	public GameScreen gs;
	
	
	@Override
	public void create () {
		gameskin = new Skin(Gdx.files.internal("uiskin.json"));
		TextureAtlas partatlas = new TextureAtlas("parts.pack");
		TextureAtlas asteroidpartatlas = new TextureAtlas("asteroidparts.pack");
		gameskin.addRegions(partatlas);
		gameskin.addRegions(asteroidpartatlas);
		effects = new Effects();
		batch = new SpriteBatch();
		es = new EditorScreen(this);
		gs = new GameScreen(this);
		setScreen(new MenuScreen(this));
		Box2D.init();
	}

	/**Die rendermethode wird durchgedend 60 mal pro Sekunde ausgef�hrt und bietet Platz f�r wiederkehrende Abl�ufe die in k�rzester Zeit berechnet werden k�nnen*/
	@Override
	public void render () {
		super.render();
		
	}
	
	
}
