
package com.hgs.desktop;

import java.awt.Dimension;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import com.hgs.ShipBuilder;

public class DesktopLauncher {
	public static void main (String[] arg) {
		
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		config.width = dimension.width;
		config.height = dimension.height;
		config.resizable = true;
		config.fullscreen = false;
		System.setProperty("org.lwjgl.opengl.Window.undecorated", "false");
		new LwjglApplication(new ShipBuilder(), config);
		
		
		
		
	}
}
